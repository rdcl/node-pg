import { Client, ClientConfig, QueryResult } from 'pg'

export type QueryOptions = {
  errorMapper?: {
    [key: string]: (error: Error) => Error,
  },
}

export type QuerySpecification = {
  select<T>(rowMapper?: (row: any) => T, options?: QueryOptions): Promise<T[]>;
  selectOne<T>(rowMapper?: (row: any) => T, options?: QueryOptions): Promise<T | undefined>;
  execute(options?: QueryOptions): Promise<void>;
}

type TemplateTag<T> = (strs: string[], ...vals: any[]) => T
type TemplateTagArgs = [string[], ...any[]]
type QueryArgs = [QueryOptions?]
type RowMapper<T> = (row: any) => T
type MappableQueryArgs<T> = [RowMapper<T>?, QueryOptions?]
type MultiResponseType<T> = Promise<T> | TemplateTag<Promise<T>>

export class PG {
  private readonly _config?: string | ClientConfig

  constructor(config?: string | ClientConfig) {
    this._config = config

    this.q = this.q.bind(this)
    this.withDb = this.withDb.bind(this)
    this.select = this.select.bind(this)
    this.selectOne = this.selectOne.bind(this)
    this.execute = this.execute.bind(this)
  }

  q(strs: string[], ...vals: any[]): QuerySpecification {
    const pg = this
    return {
      select<T>(rowMapper?: RowMapper<T>, options?: QueryOptions): Promise<T[]> {
        return pg.select<T>(rowMapper, options)(strs, ...vals)
      },
      selectOne<T>(rowMapper?: RowMapper<T>, options?: QueryOptions): Promise<T | undefined> {
        return pg.selectOne<T>(rowMapper, options)(strs, ...vals)
      },
      execute(options?: QueryOptions): Promise<void> {
        return pg.execute(options)(strs, ...vals)
      },
    }
  }

  async withDb<T>(cb: (db: Connection) => Promise<T>): Promise<T> {
    let client: Client | null = null

    try {
      client = new Client(this._config)
      await client.connect()

      return await cb(new Connection(client))
    } finally {
      if (client !== null) {
        await client.end()
      }
    }
  }

  select<T>(strs: string[], ...vals: any[]): Promise<T[]>;
  select<T>(rowMapper?: RowMapper<T>, options?: QueryOptions): TemplateTag<Promise<T[]>>;
  select<T>(...args: TemplateTagArgs | MappableQueryArgs<T>): MultiResponseType<T[]> {
    if (isTemplateTag(args)) {
      const [strs, ...vals] = args
      return this.withDb<any>(db => db.select()(strs, ...vals))
    }

    const [rowMapper, options] = args
    return (strs, ...vals) => this.withDb(db => db.select(rowMapper, options)(strs, ...vals))
  }

  selectOne<T>(strs: string[], ...vals: any[]): Promise<T | undefined>;
  selectOne<T>(rowMapper?: RowMapper<T>, options?: QueryOptions): TemplateTag<Promise<T | undefined>>;
  selectOne<T>(...args: TemplateTagArgs | MappableQueryArgs<T>): MultiResponseType<T | undefined> {
    if (isTemplateTag(args)) {
      const [strs, ...vals] = args
      return this.withDb<any>(db => db.selectOne()(strs, ...vals))
    }

    const [rowMapper, options] = args
    return (strs, ...vals) => this.withDb(db => db.selectOne(rowMapper, options)(strs, ...vals))
  }

  execute(strs: string[], ...vals: any[]): Promise<void>;
  execute(options?: QueryOptions): TemplateTag<Promise<void>>;
  execute(...args: TemplateTagArgs | QueryArgs): MultiResponseType<void> {
    if (isTemplateTag(args)) {
      const [strs, ...vals] = args
      return this.withDb(db => db.execute()(strs, ...vals))
    }

    const [options] = args
    return (strs, ...vals) => this.withDb(db => db.execute(options)(strs, ...vals))
  }
}

class Connection {

  client: Client

  constructor(client: Client) {
    this.client = client
  }

  query(strs: string[], ...vals: any[]): Promise<QueryResult>;
  query(options?: QueryOptions): TemplateTag<Promise<QueryResult>>;
  query(...args: TemplateTagArgs | QueryArgs): MultiResponseType<QueryResult> {
    if (isTemplateTag(args)) {
      const [strs, ...vals] = args
      return this.query()(strs, ...vals)
    }

    const [options = {}] = args
    return async (strs, ...vals) => {
      try {
        let query = strs[0]

        const len = strs.length
        for (let i = 1; i < len; i += 1) {
          query += '$' + i + strs[i]
        }

        return await this.client.query(query, vals)
      } catch (err) {
        if (options.errorMapper && options.errorMapper[err.code]) {
          throw options.errorMapper[err.code](err)
        }

        throw err
      }
    }
  }

  select<T>(strs: string[], ...vals: any[]): Promise<T[]>;
  select<T>(rowMapper?: RowMapper<T>, options?: QueryOptions): TemplateTag<Promise<T[]>>;
  select<T>(...args: TemplateTagArgs | MappableQueryArgs<T>): MultiResponseType<T[]> {
    if (isTemplateTag(args)) {
      const [strs, ...vals] = args
      return this.select<any>()(strs, ...vals)
    }

    const [rowMapper = identity, options = {}] = args
    return async (strs, ...vals) => {
      const result = await this.query(options)(strs, ...vals)
      return result.rows.map<T>(rowMapper)
    }
  }

  selectOne<T>(strs: string[], ...vals: any[]): Promise<T | undefined>;
  selectOne<T>(rowMapper?: RowMapper<T>, options?: QueryOptions): TemplateTag<Promise<T | undefined>>;
  selectOne<T>(...args: TemplateTagArgs | MappableQueryArgs<T>): MultiResponseType<T | undefined> {
    if (isTemplateTag(args)) {
      const [strs, ...vals] = args
      return this.selectOne<any>()(strs, ...vals)
    }

    const [rowMapper, options] = args
    return async (strs, ...vals) => {
      const rows = await this.select(rowMapper, options)(strs, ...vals)
      return rows[0]
    }
  }

  execute(strs: string[], ...vals: any[]): Promise<void>;
  execute(options?: QueryOptions): TemplateTag<Promise<void>>;
  execute(...args: TemplateTagArgs | QueryArgs): MultiResponseType<void> {
    if (isTemplateTag(args)) {
      const [strs, ...vals] = args
      return this.execute()(strs, ...vals)
    }

    const [options] = args
    return async (strs, ...vals) => {
      await this.query(options)(strs, ...vals)
    }
  }
}

function identity<T>(row: T): T {
  return row
}

function isTemplateTag(args: any[]): args is TemplateTagArgs {
  return args.length > 0 && Array.isArray(args[0])
}
